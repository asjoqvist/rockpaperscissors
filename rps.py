# coding: utf-8
#
# STEN, SAX OCH PÅSE
# ==================


# SLUMP
# =====
# För att slumpa värden kan man använda funktioner ur modulen random. Titta i
# dokementationen för denna modul.
#
# http://docs.python.org/3.3/library/random.html#module-random
#
# Bra alternativ för att lösa denna uppgift kan vara funktionerna randint eller
# sample.
#
# http://docs.python.org/3.3/library/random.html#random.randint
# http://docs.python.org/3.3/library/random.html#random.sample
def ai_move(moves):
    """Returns a random move from moves.

    Arguments:
            moves: a tuple of strings where each element is a move.

    Returns:
            one of the moves, chosen randomly.
    """


# Funktionen avgör om ett drag är giltigt. Ett drag är giltigt om det finns i
# tupeln moves.
def is_valid(move, moves):
    """Returns True if move is valid, False Otherwise.

    Arguments:
            move: a string.
            moves: a tuple of strings where each element is a move.

    Returns:
        True of False.
    """


# Denna funktion ska avgöra om en dator fuskar. Chansen för detta är argumentet
# chance, i procent.
def cheat(chance):
    """Determines if the ai cheats.

    Arguments:
            chance: the chance, in percent, of the ai cheating.

    Returns:
            True or False.
    """


def evaluate(pl_move, ai_move):
    """Determines wether player wins, ai wins or there is a tie.

    Arguments:
        pl_move: the palyer's move, a string.
        ai_move: the ai's move.
        chance: the chance of the ai cheating, in percent.

    Returns:
        1  if the player wins.
        0  if there is a tie
       -1  if the ai wins
    """


def feedback(pl_move, ai_move, win):
    """Returns a status message with results to be printed."""


def main():
    """Entry point for the program."""


if __name__ == '__main__':
    main()
